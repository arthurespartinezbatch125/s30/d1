const express = require('express');
const mongoose = require('mongoose'); //to be used on our db collection and the creation of our schema and model for our existing MongoDB atlas collection
const app = express(); //creating a server thru the use of app variable
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//mongoose.connect - way to connect mongoDB atlas db connection string to our server
//paste inside the connect() method the connection string copied from the MongoDB atlas db.
	//it must be in string value/enclosed with double/single/backticks quote
//remember to replace the password and db name with their actual values
//Due to updates made by MongoDB Atlas developers, the default connection string is being flagged as an error,
	//to skip that error or warning int the future, we will use useNewUrlParser and useUnifiedTopology
mongoose.connect("mongodb+srv://arthur25:arthurespartinez@cluster0.iymnv.mongodb.net/b125-tasks?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
).then(()=>{ //if the mongoose succeded on the connection, then we will print the message
	console.log("Succesfully Connected to Database");
}).catch((error)=>{ //handles error when the mongoose failed to connect on our mongoDB
	console.log(error);
});

/*------------------------------------------------------------*/
/*SCHEMA -gives structure of what kind of record/document we are going to contain on our database*/
// Schema() method - determines the structure of the documents to be written in the database
// SChema acts as blueprint to our data
//We used the Schema() constructor of the Mongoose dependency to create a new Schema object for our tasks collection
//The "new" keyword, creates a new Schema

const taskSchema = new mongoose.Schema({
	//Define the fields with their corresponding data type
	//for task, it needs a field called 'name' and 'status'
	//The field name has a data type of 'String'
	//The field status has a data type of 'Boolean with a deafult value of 'false
	name: String,
	status: {
		type: Boolean,
		//Default values are the predefined values for a field if we dont put any value
		default: false
	}
});

/*MODELS to perform the CRUD operation for our defined collections w/ corresponding schema*/
//The Task contain will contain the model for our tasks collection that shall perform the CRUD
//The first parameter of the mongoos.model method indicates the collection in where to store the data.
	//Take note: the collection name must be written in singular form and capitalized
//The second parameter is used to specify the Schema of the documents that will be stored on the tasks collection
const Task = mongoose.model('Task', taskSchema); 

/*-------------------------------MINI ACTIVITY----------------------*/

const userSchema = new mongoose.Schema({

	firstName: String,
	lastName: String,
	userName: String,
	password: String
});

const User = mongoose.model('User', userSchema);

/*
	BUSINESS LOGIC - Todo list application
	- CRUD operation for our task collection-
*/
//insert new task
app.post('/add-task', (req,res)=>{
	//call the model for our task collection
	//create an instance of the task model and save it to our database
	//creating new Task with a task name 'PM Break' through the use of the Task Model
	let newTask = new Task({
		name: req.body.name
	});
	//newTask will now be saved as a new document to our collection in DB
	//.save - saves a new doc to our db
	//on our callback, it will receive 2 values, the error and the saved doc
	//error shall contain error whenever there is an error while saving our doc
	//savedTask shall contain the newly saved doc from the db once saving process is successful
	newTask.save((error, savedTask)=>{
		if(error){
			console.log(error);
		} else{
			res.send(`New task saved! ${savedTask}`);
		}
	});
});
/*--------------------------MINI ACTIVITY---------------------*/
app.post('/register', (req, res)=>{
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
	});
	newUser.save((error, savedUser)=>{
		if(error){
			console.log(error);
		} else {
			res.send(savedUser);
		}
	})
})

// app.post("/add_user", async(req, res) => {
// 	const user = new userModel(req.body);
// // async & await
// // try & catch
// 	try {
// 		await user.save();
// 		res.send(user);
// 	} catch (error) {
// 		res.status(500).send(error);
// 	}

// });

//RETRIEVAL

app.get('/retrieve-tasks', (req, res)=>{
	//find({}) will retrieve all docs from tasks collection
	//the error will handle errors while retrieving
	//records will handle the raw data
	Task.find({}, (error, records)=>{
		if(error){
			console.log(error);
		} else {
			res.send(records)
		}
	});
});
//retreive tasks that are done, means the status = true
app.get('/retrieve-tasks-done', (req, res)=>{
	Task.find({status: true}, (error, records)=>{
		if(error){
			console.log(error);
		} else {
			res.send(records)
		}
	});
});

//update operation
app.put('/complete-task/:taskId', (req, res)=>{
	// res.send({urlParams: req.params.taskId})
	//1. find the specific record
	//2. update
	let taskId = req.params.taskId;
	//url parameters - theses are the values defined on the URLS
	//to get the URL parameters - req.params. <parameters>
	// ::taskId - is away to indicate that we are goinh to receive url parameter, these are what we called a "wildcard"
	Task.findByIdAndUpdate(taskId, {status: true}, (error, updatedTask)=>{
		if (error){
			console.log(error);
		} else {
			res.send(`Task completed Succesfully!`)
		}
	});
});
//DElete
app.delete('/delete-task/:taskId', (req, res)=>{
	let taskId = req.params.taskId
	Task.findByIdAndDelete(taskId, (error, deletedTask)=>{
		if(error){
			console.log(error);
		} else {
			res.send(`Task deleted!`);
		}
	});
});

app.listen(port, ()=> console.log(`Server is running at port ${port}`));